1) To run the program, execute the following command
       python Reversi.py
	   
2) I have added both minimax and alpha-beta algorithms but program uses alpha-beta. To use minimax comment out line 141 and uncomment 142.

3) Due to slowness, program searches till depth 4. To increase/decrease the search depth, change line 150