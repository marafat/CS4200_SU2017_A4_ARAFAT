import os, copy

class NReversiBoard:
    def __init__(self, N):
        self.size = N
        self.state = [['*' for x in range(N)] for y in range(N)]
        self.state[N/2-1][N/2-1] = 'B'
        self.state[N/2-1][N/2] = 'W'
        self.state[N/2][N/2-1] = 'W'
        self.state[N/2][N/2] = 'B'


class Play:
    directionX = [-1, 0, 1, -1, 1, -1, 0, 1]
    directionY = [-1, -1, -1, 0, 0, 1, 1, 1]

    def __init__(self, board):
        self.state = board.state
        self.size = board.size
        self.minEvalScore = -1  # min - 1
        self.maxEvalScore = self.size * self.size + 4 * self.size + 4 + 1  # max + 1

    def print_board(self):
        for i in xrange(self.size):
            for j in xrange(self.size):
                print self.state[i][j],
            print ' '

    def make_move(self, state, x, y, player):
        piecesCaptured = 0
        newState = copy.deepcopy(state)
        newState[y][x] = player
        for d in range(8):
            count = 0
            for i in range(self.size):
                dx = x + Play.directionX[d] * (i + 1)
                dy = y + Play.directionY[d] * (i + 1)
                if dx < 0 or dx > self.size - 1 or dy < 0 or dy > self.size  - 1:
                    count = 0
                    break
                elif newState[dy][dx] == player:
                    break
                elif newState[dy][dx] == '*':
                    count = 0
                    break
                else:
                    count += 1
            for i in range(count):
                dx = x + Play.directionX[d] * (i + 1)
                dy = y + Play.directionY[d] * (i + 1)
                newState[dy][dx] = player
                piecesCaptured += count
        return newState, piecesCaptured

    def is_valid_move(self, state, x, y, player):
        if x < 0 or x > self.size - 1 or y < 0 or y > self.size - 1:
            return False
        if state[y][x] != '*':
            return False
        (tempState, piecesCaptured) = self.make_move(state, x, y, player)
        if piecesCaptured == 0:
            return False
        return True

    def evaluate_board(self, state, player):
        total = 0
        for y in range(self.size):
            for x in range(self.size):
                if state[y][x] == player:
                    if (x == 0 or x == self.size - 1) and (y == 0 or y == self.size - 1):
                        total += 4
                    elif (x == 0 or x == self.size - 1) or (y == 0 or y == self.size - 1):
                        total += 2
                    else:
                        total += 1
        return total

    def is_terminal_node(self, state, player):
        for y in range(self.size):
            for x in range(self.size):
                if self.is_valid_move(state, x, y, player):
                    return False
        return True

    def minimax(self, state, sDepth, player, maximizingPlayer):
        if sDepth == 0 or self.is_terminal_node(state, player):
            return self.evaluate_board(state, player)
        if maximizingPlayer:
            bestValue = self.minEvalScore
            for y in range(self.size):
                for x in range(self.size):
                    if self.is_valid_move(state, x, y, player):
                        (tempState, pCapt) = self.make_move(state, x, y, player)
                        v = self.minimax(tempState, sDepth-1, player, False)
                        bestValue = max(bestValue, v)
        else:  # minimizingPlayer
            bestValue = self.maxEvalScore
            for y in range(self.size):
                for x in range(self.size):
                    if self.is_valid_move(state, x, y, player):
                        (tempState, pCapt) = self.make_move(state, x, y, player)
                        v = self.minimax(tempState, sDepth-1, player, True)
                        bestValue = min(bestValue, v)
        return bestValue


    def alpha_beta(self, state, sDepth, player, alpha, beta, maximizingPlayer):
        if sDepth == 0 or self.is_terminal_node(state, player):
            return self.evaluate_board(state, player)
        if maximizingPlayer:
            v = self.minEvalScore
            for y in range(self.size):
                for x in range(self.size):
                    if self.is_valid_move(state, x, y, player):
                        (tempState, totctr) = self.make_move(state, x, y, player)
                        v = max(v, self.alpha_beta(tempState, sDepth-1, player, alpha, beta, False))
                        alpha = max(alpha, v)
                        if beta <= alpha:
                            break
            return v
        else:  # minimizingPlayer
            v = self.maxEvalScore
            for y in range(self.size):
                for x in range(self.size):
                    if self.is_valid_move(state, x, y, player):
                        (tempState, totctr) = self.make_move(state, x, y, player)
                        v = min(v, self.alpha_beta(tempState, sDepth-1, player, alpha, beta, True))
                        beta = min(beta, v)
                        if beta <= alpha:
                            break
            return v

    def best_move(self, state, sDepth, player, type):
        maxPoints = 0
        newX = -1
        newY = -1
        for y in range(self.size):
            for x in range(self.size):
                if self.is_valid_move(state, x, y, player):
                    (tempState, pCap) = self.make_move(state, x, y, player)
                    points = self.alpha_beta(tempState, sDepth, player, self.minEvalScore, self.maxEvalScore, type)
                    #points = self.minimax(tempState, sDepth, player, type)
                    if points > maxPoints:
                        maxPoints = points
                        newX = x
                        newY = y
        return (newX, newY)

if __name__ == '__main__':
    check = 0
    boardSize = input("Enter Board Size: ")
    if boardSize < 3:
        print "Illegal Board Size. Board Size should be greater than 2"
        exit(0)
    board = NReversiBoard(boardSize)
    play = Play(board)
    print "Starting Board:"
    play.print_board()
    while True:
        maxPlayer = True
        for player in ['B', 'W']:
           if play.is_terminal_node(play.state, player):
               b=0; w=0
               for r in xrange(play.size):
                   for c in xrange(play.size):
                      if play.state[r][c] == 'B':
                          b+=1
                      if play.state[r][c] == 'W':
                          w+=1
               if b+w != pow(play.size, 2) and check <= 1:
                   check += 1
                   continue
               print "\nFinal Board:"
               play.print_board()
               print '\nPlayer B score: %d' %b
               print 'Player W score: %d' %w
               if b > w:
                   print "\nPlayer B wins!"
               elif b < w:
                   print "\nPlayer W wins!"
               else:
                   print "\n Match Drawn"
               os._exit(0)
           (x, y) = play.best_move(play.state, 4, player, maxPlayer) #only searching till depth = 4
           if not (x == -1 and y == -1):                             #else takes too much time
               (play.state, totctr) = play.make_move(play.state, x, y, player)
               maxPlayer = False
               #play.print_board()
